<?php 
	/**
	* 
	*/
	class CarClss
	{	
		public $mark;
		public $color;
		public $price;
		public function changecolor($newcolor){
			return $this->color = $newcolor;
		}
	}

	class Tv {
		public function __construct($manufacturer,$price,$discount=0)
			{
				$this->manufacturer=$manufacturer;
				$this->price=$price;
				$this->discount=$discount;
			}
		private $cost;
		public function GetCost(){
			$this->cost = $this->price - ($this->price*$this->discount/100);
		}
	}		

	class Pen {
		public function __construct($name, $color)
			{
				$this->name = $name;
				$this->color = $color;
			}
	}

	class Duck 
	{
		
		function __construct($name, $type)
		{
			$this->name = $name;
			$this->type = $type;
		}
	}

	class Product {
		function __construct($name, $id, $price){
			$this->name = $name;
			$this->id = $id;
			$this->price = $price;
		}
	}

	class News 
	{
		
		public function __construct($header, $content, $date)
		{
			$this->header = $header;
			$this->content = $content;
			$this->date = $date;
		}
		public function FormNews() {
			$header = $this->header;
			$content = $this->content;
			$date = $this->date;
			echo "<h2>$header</h2>";
			echo "<p>$content</p>";
			echo '<p> Дата новости:'.$date.'</p>';
		}
		public function GetComments($commentObj) {
			$commentObj -> CommentPrint();
		}
	}

	/**
	* 
	*/
	class Comments
	{
		public function __construct($login, $comment, $date){
			$this->login = $login;
			$this->comment = $comment;
			$this->date = $date;

		}
		public function CommentPrint() {
			$login = $this->login;
			$comment = $this->comment;
			$date = $this->date;
			echo "<strong>$login</strong> оставил комментарий: <br>";
			echo "<p>$comment</p>";
			echo "<span>$date</span>";
		}
	}

	$toyota = new CarClss();
	$toyota->mark = 'Toyota';
	$toyota->color = 'Green';
	$toyota->price = 100500;

	$audi = new CarClss();
	$audi->mark = 'Audi';
	$audi->color = 'White';
	$audi->price = 200500;

	$samsung = new Tv('Samsung', 100, 30);
	$samsung->GetCost();

	$panasonic = new Tv('Panasonic', 300, 5);
	$panasonic->GetCost();

	$penPilot = new Pen('Pilot', 'Синяя');
	$penLiner = new Pen('Liner', 'Черная');

	$duck1 = new Duck('Скрудж', 'Под лимонным соусом');
	$duck2 = new Duck('Вилли', 'В яблоках');

	$comp1 = new Product('Pentium', '123123', 100500);
	$comp2 = new Product('Apple Air', '3213112', 500500);

	$news1 = new News('что-то случилось', 'сама длинная суть того, что случилось что-то хорошее или плохое', '11/12/1913');
	$news2 = new News('еще одна новость', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore provident possimus, accusamus ea est asperiores illum eius iste, dolores modi beatae assumenda corrupti culpa, saepe exercitationem molestias cumque! Quidem, illo.', '11/12/2010');

	$comment1 = new Comments('Рома', 'Да ерунда это всё!', '11/14/1229');
	$comment2 = new Comments ('Аноним', 'Это кажется работает!', '12/14/5663');

 ?>